package fau.android.greenwise;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class HomeScreen extends Activity implements OnClickListener {

	private String school;
	private int grade;

	private static final String TAG_SCHOOL = "school_string";
	private static final String TAG_GRADE = "grade";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_screen);
		addListeners();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main_menu, menu);
		return true;
	}

	public void addListeners() {
		View startButton = findViewById(R.id.start_button);
		startButton.setOnClickListener(this);
		View ruleButton = findViewById(R.id.map_button);
		ruleButton.setOnClickListener(this);
		View exitButton = findViewById(R.id.exit_button);
		exitButton.setOnClickListener(this);
		View statButton = findViewById(R.id.stats_button);
		statButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.start_button:
			openNewGameDialog();
			break;
		case R.id.map_button:
			openMap();
			break;
		case R.id.stats_button:
			Intent stat = new Intent(this, StatsPage.class);
			startActivity(stat);
		case R.id.exit_button:
			finish();
			break;
		}
	}

	private void openNewGameDialog() {
		new AlertDialog.Builder(this)
				.setTitle(R.string.new_game_school)
				.setItems(R.array.schools,
						new DialogInterface.OnClickListener() {
							public void onClick(
									DialogInterface dialoginterface, int i) {
								selectSchool(i);
							}
						}).show();
	}

	private void openMap() {
		Intent i = new Intent(this, Map.class);
		startActivity(i);
	}

	private void selectSchool(int i) {
		if (i == 0) {
			school = getString(R.string.school0);
		} else if (i == 1) {
			school = getString(R.string.school1);
		} else if (i == 2) {
			school = getString(R.string.school2);
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this).setTitle(R.string.new_game_grade)
				.setItems(R.array.grade, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int i) {
						selectGrade(i);
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void selectGrade(int i) {
		if (i == 0) {
			grade = 0;
		} else if (i == 1) {
			grade = 1;
		} else if (i == 2) {
			grade = 2;
		} else if (i == 3) {
			grade = 3;
		} else if (i == 4) {
			grade = 4;
		} else if (i == 5) {
			grade = 5;
		}
		startGame();
	}

	private void startGame() {
		Intent newGame = new Intent(this, OnePlayer.class);
		newGame.putExtra(TAG_SCHOOL, school);
		newGame.putExtra(TAG_GRADE, grade);
		startActivity(newGame);
	}
}
