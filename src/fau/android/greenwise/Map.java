package fau.android.greenwise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class Map extends Activity {
	private GoogleMap mMap;
	static final LatLng PARKRIDGE = new LatLng(26.294863, -80.110032);
	static final LatLng DEERFIELDPARK = new LatLng(26.3090391, -80.1073045);
	static final LatLng TEDDER = new LatLng(26.2814395, -80.1240972);

	JSONParser jParser = new JSONParser();

	private static String url_pull_average = "lamp.cse.fau.edu/~jahring1/greenwisekids/pullaveragescores.php";

	private static final String TAG_SUCCESS = "success";
	private static final String TAG_TOTAL = "total";
	private static final String TAG_ITEM = "item";
	private static final String TAG_SCHOOL_ID = "school_id";
	private static final String TAG_SCHOOL_STRING = "school_string";
	JSONArray items = null;
	ArrayList<HashMap<String, String>> schoolList;
	
	private String norcrest;
	private String deerfield;
	private String parkridge;
	
	private int norcrestTotal;
	private int deerfieldTotal;
	private int parkridgeTotal;

	Marker mParkRidge;
	Marker mDeerfield;
	Marker mNorcrest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		schoolList = new ArrayList<HashMap<String, String>>();
		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();
		mMap.setMyLocationEnabled(true);
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(PARKRIDGE, 13));
		
		mParkRidge = mMap.addMarker(new MarkerOptions().position(PARKRIDGE));
		mDeerfield = mMap.addMarker(new MarkerOptions().position(DEERFIELDPARK));
		mNorcrest = mMap.addMarker(new MarkerOptions().position(TEDDER));
		
		new LoadAllSchools().execute();
	}
	
	private void setMarkers() {
		mParkRidge.setTitle(parkridge + ": " + parkridgeTotal);
		mParkRidge.setSnippet("");
		mDeerfield.setTitle(deerfield + ": " + deerfieldTotal);
		mDeerfield.setSnippet("");
		mNorcrest.setTitle(norcrest + ": " + norcrestTotal);
		mNorcrest.setSnippet("");
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	class LoadAllSchools extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... args) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			JSONObject json = jParser.makeHttpRequest(url_pull_average, "GET",
					params);
			Log.d("All schools: ", json.toString());
			try {
				int success = json.getInt(TAG_SUCCESS);
				if (success == 1) {
					items = json.getJSONArray(TAG_ITEM);
					for (int i = 0; i < items.length(); i++) {
						JSONObject c = items.getJSONObject(i);

						int id = Integer.parseInt(c.getString(TAG_SCHOOL_ID));
						String name = c.getString(TAG_SCHOOL_STRING);
						int total = c.getInt(TAG_TOTAL);

						HashMap<String, String> map = new HashMap<String, String>();
						map.put(TAG_TOTAL, String.valueOf(total));
						map.put(TAG_SCHOOL_ID, String.valueOf(id));
						map.put(TAG_SCHOOL_STRING, name);

						schoolList.add(map);
					}
				} else {
					// No schools found. Don't do anything.
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}
		protected void onPostExecute(String file_url) {
			parkridge = schoolList.get(0).get(TAG_SCHOOL_STRING);
			deerfield = schoolList.get(1).get(TAG_SCHOOL_STRING);
			norcrest = schoolList.get(2).get(TAG_SCHOOL_STRING);
			
			parkridgeTotal = Integer.parseInt(schoolList.get(0).get(TAG_TOTAL));
			deerfieldTotal = Integer.parseInt(schoolList.get(1).get(TAG_TOTAL));
			norcrestTotal = Integer.parseInt(schoolList.get(2).get(TAG_TOTAL));
			setMarkers();
		}
	}
}
