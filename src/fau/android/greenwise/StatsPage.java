package fau.android.greenwise;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class StatsPage extends Activity implements OnClickListener {

	Button view_schools;
	Button view_kid;
	Button main_menu;
	public static final String EXTRA_KID_ID = "com.example.connectingdatabase.MESSAGE";
	private ProgressDialog pDialog;

	JSONParser jsonParser = new JSONParser();

	private static String url_save_kid = "lamp.cse.fau.edu/~jahring1/greenwisekids/savekidscore.php";
	//private static final String TAG_SUCCESS = "success";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stats);

		view_schools = (Button) findViewById(R.id.my_stats);
		view_kid = (Button) findViewById(R.id.view_school_average);
		main_menu = (Button) findViewById(R.id.main_menu);

		view_schools.setOnClickListener(this);
		view_kid.setOnClickListener(this);
		main_menu.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.stats, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.my_stats:
			// intent to new activity that shows listview of all the
			// schools and when clicked, shows the averages
			Intent singleKid = new Intent(this, ViewSingleKid.class);
			startActivity(singleKid);
			break;
		case R.id.view_school_average:
			// Grab id from edit text and display new activity with unique kid
			// scores
			Intent schoolList = new Intent(this, AllSchools.class);
			startActivity(schoolList);
			break;
		case R.id.main_menu:
			Intent i = new Intent(this, HomeScreen.class);
			startActivity(i);
		}

	}

	class UploadKid extends AsyncTask<String, String, String> {

		private int getRandSub() {
			Random generator = new Random();
			int i = generator.nextInt(8) + 1;
			return i;
		}

		private int getRandGrade() {
			Random generator = new Random();
			int i = generator.nextInt(5) + 1;
			return i;
		}

		private String getRandSchool() {
			String[] schools = { "PointView", "Mirror Lake", "LakeView",
					"South Harlem", "Sheridan Elementary", "Peters", "Central Park" };
			Random generator = new Random();
			return schools[generator.nextInt(7)];
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(StatsPage.this);
			pDialog.setMessage("Uploading new kid information...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// String kid_id;
			String grade = Integer.toString(getRandGrade());
			String school_string = getRandSchool();
			String energy = Integer.toString(getRandSub());
			String water = Integer.toString(getRandSub());
			String recycle = Integer.toString(getRandSub());
			String transportation = Integer.toString(getRandSub());
			String total = Integer.toString(Integer.parseInt(energy)
					+ Integer.parseInt(water) + Integer.parseInt(recycle)
					+ Integer.parseInt(transportation));

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// params.add(new BasicNameValuePair("kid_id", kid_id));
			params.add(new BasicNameValuePair("grade", grade));
			params.add(new BasicNameValuePair("school", school_string));
			params.add(new BasicNameValuePair("energy", energy));
			params.add(new BasicNameValuePair("water", water));
			params.add(new BasicNameValuePair("recycle", recycle));
			params.add(new BasicNameValuePair("transportation", transportation));
			params.add(new BasicNameValuePair("total_score", total));

			JSONObject json = jsonParser.makeHttpRequest(url_save_kid, "POST",
					params);
			Log.d("Create response", json.toString());
			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
		}
	}
}
