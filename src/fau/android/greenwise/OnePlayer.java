package fau.android.greenwise;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class OnePlayer extends Activity implements OnClickListener {
	private int turnCount = 0;
	private Integer transScore = 0;
	private Integer energyScore = 0;
	private Integer waterScore = 0;
	private Integer recycleScore = 0;
	private Integer score = 0;
	private String answer = "";
	private String trans = "trans";
	private String energy = "energy";
	private String water = "water";
	private String recycle = "recycle";
	private String questionType = "";
	private ArrayList<quoteClass> quotes = new ArrayList<quoteClass>();
	Random generator = new Random();

	private ProgressDialog pDialog;
	JSONParser jsonParser = new JSONParser();

	private static String url_save_kid = "lamp.cse.fau.edu/~jahring1/greenwisekids/savekidscore.php";

	private static final String TAG_SCHOOL = "school_string";
	private static final String TAG_GRADE = "grade";

	private String school;
	private int grade;

	ImageButton choice1;
	ImageButton choice2;
	ImageButton choice3;
	Button choice3NotImage;
	Button choice4;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_one_player);
		choice3NotImage = (Button) findViewById(R.id.choice3_button_not_image);
		choice1 = (ImageButton) findViewById(R.id.choice1_button);
		choice2 = (ImageButton) findViewById(R.id.choice2_button);
		choice3 = (ImageButton) findViewById(R.id.choice3_button);
		choice4 = (Button) findViewById(R.id.choice4_button);
		Intent i = getIntent();
		school = i.getStringExtra(TAG_SCHOOL);
		grade = i.getIntExtra(TAG_GRADE, -1);
		addListeners();
		loadQuestionDB();
		loadQuestion();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main_menu, menu);
		return true;
	}

	public void addListeners() {
		TextView tv = (TextView) findViewById(R.id.score_textview);
		tv.setText("Player score: " + score);

		choice1.setOnClickListener(this);
		choice2.setOnClickListener(this);
		choice3.setOnClickListener(this);
		choice4.setOnClickListener(this);
		choice3NotImage.setOnClickListener(this);
	}

	public void loadQuestionDB() {
		{
			try {
				InputStream is = getResources().openRawResource(R.raw.quotedb);
				DataInputStream in = new DataInputStream(is);
				BufferedReader br = new BufferedReader(
						new InputStreamReader(in));
				String a1, a2, a3, a4, q, qt, key;

				while ((br.readLine()) != null) {
					q = br.readLine();
					a1 = br.readLine();
					a2 = br.readLine();
					a3 = br.readLine();
					a4 = br.readLine();
					key = br.readLine();
					qt = br.readLine();

					System.out.println(q + " " + a1 + " " + a2 + " " + a3 + " "
							+ a4 + " " + key + " " + qt);
					quoteClass temp = new quoteClass(q, a1, a2, a3, a4, key, qt);
					quotes.add(temp);
				}

				in.close();
			}

			catch (Exception e) {
				System.err.println("Error: " + e.getMessage());
			}
		}
	}

	public void loadQuestion() {
		int rand = generator.nextInt(quotes.size());
		quoteClass temp = quotes.get(rand);

		while (temp.isChosen() == true) {
			rand = generator.nextInt(quotes.size());
			temp = quotes.get(rand);
		}

		temp.getQuestion();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.choice1_button:
			checkAnswer("a");
			break;
		case R.id.choice2_button:
			checkAnswer("b");
			break;
		case R.id.choice3_button:
			checkAnswer("c");
			break;
		case R.id.choice4_button:
			checkAnswer("d");
			break;
		case R.id.choice3_button_not_image:
			checkAnswer("c");
			break;
		}
	}

	public class quoteClass {
		private String question;
		private String answer1;
		private String answer2;
		private String answer3;
		private String answer4;
		private String keyValue;
		private String type;
		private Boolean hasBeenChosen;

		quoteClass() {
			question = "";
			answer1 = "";
			answer2 = "";
			answer3 = "";
			answer4 = "";
			keyValue = "";
			type = "";

			hasBeenChosen = false;
		}

		quoteClass(String q, String a1, String a2, String a3, String a4,
				String key, String qt) {
			question = q;
			answer1 = a1;
			answer2 = a2;
			answer3 = a3;
			answer4 = a4;
			keyValue = key;
			type = qt;
			hasBeenChosen = false;
		}

		public void getQuestion() {
			TextView questionBox = (TextView) findViewById(R.id.question_textview);
			TextView choice4text = (TextView) findViewById(R.id.choice4_button);

			questionBox.setText(question);
			if (answer1.contains(".png")) {
				choice1.setVisibility(View.VISIBLE);
				answer1 = answer1.substring(0, answer1.length() - 4);
				int id = getResources().getIdentifier(answer1, "drawable",
						getPackageName());
				Drawable d = getResources().getDrawable(id);
				choice1.setBackgroundDrawable(d);
			} else {
				choice1.setVisibility(View.GONE);
			}
			if (answer2.contains(".png")) {
				choice2.setVisibility(View.VISIBLE);
				answer2 = answer2.substring(0, answer2.length() - 4);
				int id = getResources().getIdentifier(answer2, "drawable",
						getPackageName());
				Drawable d = getResources().getDrawable(id);
				choice2.setBackgroundDrawable(d);
			} else {
				choice2.setVisibility(View.GONE);
			}
			if (answer3.contains(".png")) {
				choice3NotImage.setVisibility(View.GONE);
				choice3.setVisibility(View.VISIBLE);
				answer3 = answer3.substring(0, answer3.length() - 4);
				int id = getResources().getIdentifier(answer3, "drawable",
						getPackageName());
				Drawable d = getResources().getDrawable(id);
				choice3.setBackgroundDrawable(d);
			} else {
				choice3.setVisibility(View.GONE);
				choice3NotImage.setVisibility(View.VISIBLE);
				choice3NotImage.setText(answer3);
			}

			choice4text.setText(answer4);

			answer = keyValue;
			questionType = type;
			hasBeenChosen = true;
		}

		public void resetChosen() {
			hasBeenChosen = false;
		}

		public boolean isChosen() {
			return hasBeenChosen;
		}
	}

	public static int getStringIdentifier(Context context, String name) {
		return context.getResources().getIdentifier(name, "raw",
				context.getPackageName());
	}

	public void checkAnswer(String check) {

		if (turnCount < 7) {
			if (check.equals(answer)) {
				new AlertDialog.Builder(this)
						.setTitle(R.string.correct)
						.setItems(R.array.returnToGame,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
									}
								}).show();
				if (questionType.equals(energy)) {
					energyScore++;
				} else if (questionType.equals(trans)) {
					transScore++;
				} else if (questionType.equals(water)) {
					waterScore++;
				} else if (questionType.equals(recycle)) {
					recycleScore++;
				}
				score++;

				TextView tv = (TextView) findViewById(R.id.score_textview);
				tv.setText("Player score: " + score);

				turnCount++;
				loadQuestion();
			} else {
				new AlertDialog.Builder(this)
						.setTitle(R.string.incorrect)
						.setItems(R.array.returnToGame,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
									}
								}).show();

				turnCount++;
				loadQuestion();
			}
		} else {
			if (check.equals(answer)) {
				score++;
				if (questionType.equals(energy)) {
					energyScore++;
				} else if (questionType.equals(trans)) {
					transScore++;
				} else if (questionType.equals(water)) {
					waterScore++;
				} else if (questionType.equals(recycle)) {
					recycleScore++;
				}
				String endScore = score.toString();

				new AlertDialog.Builder(this)
						.setTitle(
								"Correct Green Wise Kid! "
										+ "Your final score is " + endScore)
						.setItems(R.array.endGame,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
										restartGame(i);
									}
								}).show().setCanceledOnTouchOutside(false);

				TextView tv = (TextView) findViewById(R.id.score_textview);
				tv.setText("Player score: " + score);
			} else {
				String endScore = (score.toString());

				new AlertDialog.Builder(this)
						.setTitle(
								"Sorry try and be more GREEN! "
										+ "Your final score is " + endScore)
						.setItems(R.array.endGame,
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialoginterface,
											int i) {
										restartGame(i);
									}
								}).show().setCanceledOnTouchOutside(false);
			}
		}
	}

	public void restartGame(int i) {
		if (i == 0) {
			turnCount = 0;
			score = 0;
			energyScore = 0;
			transScore = 0;
			waterScore = 0;
			recycleScore = 0;

			TextView tv = (TextView) findViewById(R.id.score_textview);
			tv.setText("Player score: " + score);

			for (int j = 0; j < quotes.size(); j++) {
				quoteClass temp = quotes.get(j);
				temp.resetChosen();
			}

			loadQuestion();
		} else if (i == 1) {
			new UploadKid().execute();
			Intent newgame = new Intent(this, HomeScreen.class);
			startActivity(newgame);
		} else if (i == 2) {
			Intent newgame = new Intent(this, HomeScreen.class);
			startActivity(newgame);
		}
	}

	class UploadKid extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(OnePlayer.this);
			pDialog.setMessage("Uploading new kid information...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// String kid_id;
			String s_grade = String.valueOf(grade);
			String s_school_string = school;
			String s_energy = energyScore.toString();
			String s_water = waterScore.toString();
			String s_recycle = recycleScore.toString();
			String s_transportation = transScore.toString();
			String s_total = score.toString();

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// params.add(new BasicNameValuePair("kid_id", kid_id));
			params.add(new BasicNameValuePair("grade", s_grade));
			params.add(new BasicNameValuePair("school", s_school_string));
			params.add(new BasicNameValuePair("energy", s_energy));
			params.add(new BasicNameValuePair("water", s_water));
			params.add(new BasicNameValuePair("recycle", s_recycle));
			params.add(new BasicNameValuePair("transportation",
					s_transportation));
			params.add(new BasicNameValuePair("total_score", s_total));

			JSONObject json = jsonParser.makeHttpRequest(url_save_kid, "POST",
					params);
			Log.d("Create response", json.toString());
			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
		}
	}

}
